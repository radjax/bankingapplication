﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1 {
    class Program {
        static void Main(string[] args) {

            int mainMenu = 0;
            User user = null;
            do {
                mainMenu = DisplayMenu();
                user = LoginEntry(mainMenu);
            } while (mainMenu != 3);

        }

        static public void AccountingEntry(int selection, User user) {
            Account account = LoginRepository.GetAccount(user.UserId);
            switch (selection) {
                case 0:
                    Console.WriteLine("Invalid entry, please enter a numeric value.");
                    break;
                case 1:
                    decimal depositBalanceCheck = 0.00m;
                    Console.WriteLine("Enter Amount:");
                    string depositAmount = Console.ReadLine();
                    decimal.TryParse(depositAmount, out depositBalanceCheck);
                    if (depositBalanceCheck != 0.00m) {
                        decimal depositBalance = LoginRepository.DepositFunds(account.AccountId, Convert.ToDecimal(depositAmount));
                        Console.WriteLine("Your new account balance is: " + depositBalance.ToString("C2"));
                    } else {
                        Console.WriteLine("Invalid entry, please enter a numeric value.");
                    }
                    break;
                case 2:
                    decimal withdrawBalanceCheck = 0.00m;
                    Console.WriteLine("Enter Amount:");
                    string withdrawAmount = Console.ReadLine();
                    decimal.TryParse(withdrawAmount, out withdrawBalanceCheck);
                    if (withdrawBalanceCheck != 0.00m) {
                        decimal withdrawBalance = LoginRepository.WithdrawFunds(account.AccountId, Convert.ToDecimal(withdrawAmount));
                        Console.WriteLine("Your new account balance is: " + withdrawBalance.ToString("C2"));
                    } else {
                        Console.WriteLine("Invalid entry, please enter a numeric value.");
                    }
                    break;
                case 3:
                    decimal currentBalance = LoginRepository.GetBalance(account.AccountId);
                    Console.WriteLine("Your account balance is: " + currentBalance.ToString("C2"));
                    break;
                case 4:
                    List<Transaction> transHistory = LoginRepository.GetTransactions(account.AccountId);
                    foreach (Transaction transaction in transHistory) {
                        Console.WriteLine(transaction.TransactionType + " " + transaction.TransactionAmount + " " + transaction.TransactionDate);
                    }
                    break;
                case 5:
                    break;
                default:
                    Console.WriteLine("Selection out of bounds, please select a number from 1 to 5");
                    break;
            }
        }
        
        static public User LoginEntry(int selection) {
            User user = null;
            switch (selection) {
                case 0:
                    Console.WriteLine("Invalid entry, please enter a numeric value.");
                    break;
                case 1:
                    Console.WriteLine();
                    Console.WriteLine("Create Account:");
                    Console.WriteLine();
                    Console.WriteLine("Enter Username");
                    string createUsername = Console.ReadLine();
                    Console.WriteLine("Enter Password");
                    string createPassword = Console.ReadLine();
                    Console.WriteLine("Enter First Name");
                    string firstName = Console.ReadLine();
                    Console.WriteLine("Enter Last Name");
                    string lastName = Console.ReadLine();
                    User userCreated = LoginRepository.CreateUser(createUsername, createPassword, firstName, lastName);
                    if(userCreated != null) {
                        Console.Clear();
                        Console.WriteLine("Account created! Log in to access account.");
                    } else {
                        Console.Clear();
                        Console.WriteLine("All fields required, please enter values in all fields.");
                    }
                    break;
                case 2:
                    Console.WriteLine();
                    Console.WriteLine("Login:");
                    Console.WriteLine();
                    Console.WriteLine("Enter Username");
                    string loginUsername = Console.ReadLine();
                    Console.WriteLine("Enter Password");
                    string loginPassword = Console.ReadLine();
                    user = LoginRepository.Login(loginUsername, loginPassword);
                    if (user != null) {
                        Console.Clear();
                        Console.WriteLine("Welcome, " + user.FirstName + "!");
                        int accountMenu = 0;
                        do {
                            accountMenu = AccountMenu();
                            AccountingEntry(accountMenu, user);
                        } while (accountMenu != 5);
                    } else {
                        Console.Clear();
                        Console.WriteLine("Could not find this user/password combination, please try again");
                        Console.WriteLine();
                    }
                    break;
                case 3:
                    Console.WriteLine("Have a great day!");
                    Console.ReadKey();
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Selection out of bounds, please enter a number from 1 to 3");
                    break;
            }
            return user;
        }

        static public int DisplayMenu() {
            Console.WriteLine();
            Console.WriteLine("Account Manager");
            Console.WriteLine();
            Console.WriteLine("1. Create Account");
            Console.WriteLine("2. Login");
            Console.WriteLine("3. Exit");
            string result = Console.ReadLine();
            int resultCheck = 0;
            int.TryParse(result, out resultCheck);
            return resultCheck;
        }


        static public int AccountMenu() {
            Console.WriteLine();
            Console.WriteLine("Bank Manager");
            Console.WriteLine();
            Console.WriteLine("1. Deposit Funds");
            Console.WriteLine("2. Withdraw Funds");
            Console.WriteLine("3. Check Balance");
            Console.WriteLine("4. Transaction History");
            Console.WriteLine("5. Logout");
            string result = Console.ReadLine();
            int resultCheck = 0;
            int.TryParse(result, out resultCheck);
            return resultCheck;
        }
    }
}
