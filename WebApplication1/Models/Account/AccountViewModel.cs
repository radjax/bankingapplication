﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models {
    public class AccountViewModel {
        public Guid AccountId { get; set; }
        public Guid UserId { get; set; }
        public decimal AccountBalance { get; set; }
        public List<Transaction> TransactionHistory { get; set; }
        public string FirstName { get; set; }
        public string Message { get; set; }
    }
}