﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication1.Models;
using WebApplication1.Models.Home;

namespace WebApplication1.Controllers {
    public class HomeController : Controller {

        public ActionResult Index() {
            return View();
        }

        [HttpGet]
        public ActionResult Login() {
            ViewBag.Message = "Login";
            LoginViewModel viewModel = new LoginViewModel();
            return View(viewModel);
        }

        public ActionResult Login(LoginViewModel viewModel) {
            viewModel.Message = "Login";
            User user = LoginRepository.Login(viewModel.Username, viewModel.Password);
            if(user != null) {
                FormsAuthentication.SetAuthCookie(viewModel.Username, false);
                return RedirectToAction("Index", "Account", new {userId = user.UserId });
            } else {
                viewModel.Message = "Could not find account, please try again.";
                return View("Login", viewModel);
            }
        }

        [HttpGet]
        public ActionResult CreateAccount() {
            CreateViewModel viewModel = new CreateViewModel();
            viewModel.Message = "Create Account";
            return View(viewModel);
        }

        public ActionResult CreateAccount(CreateViewModel viewModel) {
            ViewBag.Message = "User Created! Log in to access account.";
            if(string.IsNullOrWhiteSpace(viewModel.Username) || string.IsNullOrWhiteSpace(viewModel.Password) || string.IsNullOrWhiteSpace(viewModel.FirstName) || string.IsNullOrWhiteSpace(viewModel.LastName)) {
                viewModel.Message = "All fields required, please enter valid data in all fields.";
                return View(viewModel);
            }
            User newAccount = LoginRepository.CreateUser(viewModel.Username, viewModel.Password, viewModel.FirstName, viewModel.LastName);
            return View("Index");
        }

        
        public ActionResult Logout() {
            ViewBag.Message = "Thanks! Have a great Day!.";
            FormsAuthentication.SignOut();
            return View("Index");
        }
    }
}