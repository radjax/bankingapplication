﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AccountController : Controller
    {
        [Authorize]
        public ActionResult Index(Guid userId)
        {
            AccountViewModel viewModel = CreateAccountViewModel(userId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult Account(Guid userId) {
            AccountViewModel viewModel = CreateAccountViewModel(userId);
            return View(viewModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Deposit(Guid userId) {
            AccountViewModel viewModel = CreateAccountViewModel(userId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult Deposit(string amount, Guid userId) {
            AccountViewModel viewModel = CreateAccountViewModel(userId);
            decimal parseAmount = 0.00m;
            decimal.TryParse(amount, out parseAmount);
            if (parseAmount > 0.00m) {
                decimal balance = LoginRepository.DepositFunds(viewModel.AccountId, parseAmount);
                viewModel.AccountBalance = balance;
                viewModel.Message = "Your new account balance is: " + balance.ToString("C2");
            } else {
                viewModel.Message = "Please Enter a valid amount";
            }
            return View(viewModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Withdraw(Guid userId) {
            AccountViewModel viewModel = CreateAccountViewModel(userId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult Withdraw(string amount, Guid userId) {
            AccountViewModel viewModel = CreateAccountViewModel(userId);
            decimal currentBalance = LoginRepository.GetBalance(viewModel.AccountId);
            decimal parseAmount = 0.00m;
            decimal.TryParse(amount, out parseAmount);
            if (parseAmount > 0.00m && parseAmount < currentBalance) {
                decimal balance = LoginRepository.WithdrawFunds(viewModel.AccountId, parseAmount);
                viewModel.AccountBalance = balance;
                viewModel.Message = "Your new account balance is: " + balance.ToString("C2");
            } else if(parseAmount > 0.00m && parseAmount > currentBalance) {
                viewModel.Message = "Insufficient funds for withdrawal";
            } else {
                viewModel.Message = "Please Enter a valid amount";
            }
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ViewBalance(Guid userId) {
            AccountViewModel viewModel = CreateAccountViewModel(userId);
            return View(viewModel);
        }

        [Authorize]
        public ActionResult ViewTransactions(Guid userId) {
            AccountViewModel viewModel = CreateAccountViewModel(userId);
            if(viewModel.TransactionHistory.Count == 0) {
                viewModel.Message = "No Transaction History.";
            }
            return View(viewModel);
        }

        private AccountViewModel CreateAccountViewModel(Guid userId) {
            User user = LoginRepository.GetUser(userId);
            Account account = LoginRepository.GetAccount(userId);
            AccountViewModel viewModel = new AccountViewModel() {
                AccountId = account.AccountId,
                UserId = userId,
                AccountBalance = account.AccountBalance,
                TransactionHistory = account.TransactionHistory,
                FirstName = user.FirstName
            };

            return viewModel;
        }
    }
}